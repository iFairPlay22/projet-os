package main;

import bus.Bus;

public class Main {
	
	private static void actions() {
		Bus bus = new Bus();
		bus.add(0, "Carte son");
		bus.add(7, "Carte graphique");
		bus.add(8, "Clé USB");
		System.out.println(bus);
	}

	public static void main(String[] args) {
		try {		
			actions();
		} catch (Exception e) {
			System.out.println("An error occured!");
			e.printStackTrace();
		}
	}

}
