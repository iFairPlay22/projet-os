package bus;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import parameters.Constants;

public class Bus {
	private final List<Card> cards;
	
	private int actualDma = 0;
	private int actualInputAddress = Constants.beginInputAddress;
	private int actualOutputAddress = Constants.beginOutputAddress;

	public Bus() {
		cards = new ArrayList<Card>();
		for (int i = 0; i < Constants.busLength; i++)
			cards.add(null);
	}
	
	private int requireValidIndex(int port) {
		if (!(0 <= port && port < Constants.busLength))
			throw new IllegalArgumentException("Port number does not exists!");
		return port;
	}
	
	public void add(int port, String portName) {
		if (cards.get(requireValidIndex(port)) != null)
			throw new IllegalArgumentException("Port number already occuped!");
		cards.set(port, new Card(portName));
	}
	
	public void remove(int port) {
		if (cards.get(requireValidIndex(port)) == null)
			throw new IllegalArgumentException("Port number already occuped!");
		cards.set(port, null);
	}

	@Override
	public String toString() {
		StringBuilder res = new StringBuilder("Bus ISA sur 16 bits\n\n");
		for (int i = 0; i < Constants.busLength; i++) {
			Card card = cards.get(i);
			if (card != null) {
				res.append(i);
				res.append(") ");
				res.append(card);
				res.append("\n");
			}
		}
		return res.toString();
	}
	
	private class Card {
		private final String name;
		private final DMA dma;
		private final InputAddress inputAddress;
		private final OutputAddress outputAddress;
		
		public Card(String name) {
			this.name = Objects.requireNonNull(name);
			this.dma = new DMA(actualDma++);
			this.inputAddress = new InputAddress(actualInputAddress++);
			this.outputAddress = new OutputAddress(actualOutputAddress++);
		}
		
		@Override
		public String toString() {
			return name + " [" + dma + ", " + inputAddress + ", " + outputAddress + "]";
		}
		
		private class DMA {
			
			private final int id;

			public DMA(int id) {
				if (!(0 <= id && id < Constants.busLength))
					throw new IllegalArgumentException("Invalid DMA number!");
				this.id = id;
			}

			@Override
			public String toString() {
				return "DMA" + id;
			}
		}
		
		private class InputAddress {
			private final int address;
			
			public InputAddress(int address) {
				if (!(Constants.beginInputAddress <= address && address <= Constants.endInputAddress))
					throw new IllegalArgumentException("Input adress can't be assigned!");
				this.address = address;
			}
			
			@Override
			public String toString() {
				return "I : " + address;
			}
		}

		private class OutputAddress {
			
			private final int address;
			
			public OutputAddress(int address) {
				if (!(Constants.beginOutputAddress <= address && address <= Constants.endOutputAddress))
					throw new IllegalArgumentException("Output adress can't be assigned!");
				this.address = address;
			}

			@Override
			public String toString() {
				return "O : " + address;
			}
		}
	}

	
}
