package parameters;

public class Constants {

	public static final int beginInputAddress = 100;
	public static final int endInputAddress = 200;
	
	public static final int beginOutputAddress = 200;
	public static final int endOutputAddress = 300;
	
	public static final int busLength = 16;
	
	public static final int dmaLength = 8;

}
